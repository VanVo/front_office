*** Settings ***
Resource  ../Resources/FrontOfficeAPP.robot
Resource  ../Resources/CommonWeb.robot

Test Setup  Begin The Test
Test Teardown  End The Test

*** Variables ***
${BROSWER} =  gc
${START_URL} =  http://www.robotframeworktutorial.com/front-office/


*** Test Cases ***
Should be access Team page
    [Tags]  Smoke_test
    [Documentation]  This is testcase 1
    FrontOfficeAPP.Go to Landing page
    FrontOfficeAPP.Go to "Team" page

Team page should match requirements
    [Tags]  Smoke_test
    [Documentation]  This is testcase 2
    FrontOfficeAPP.Go to Landing page
    FrontOfficeAPP.Go to "Team" page
    FrontOfficeAPP.Validate "Team" page