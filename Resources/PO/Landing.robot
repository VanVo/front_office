*** Settings ***
Library  Selenium2Library


*** Variables ***
${LANDING_NAVIGATION} =  xpath=//*[@id='mainNav']/div/div[1]/a

*** Keywords ***
Navigate to
    Go To  ${START_URL}

Verify Page Load
    wait until page contains element  ${LANDING_NAVIGATION}

