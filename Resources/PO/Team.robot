*** Settings ***
Library  Selenium2Library


*** Variables ***
${TEAM_HEADER_LABEL} =  //*[@id='team']/div/div[1]/div/h2

*** Keywords ***
Verify Page Load
    wait until page contains element  ${TEAM_HEADER_LABEL}

Validate "Team" Page Content
    ${ElementText} =  get text  ${TEAM_HEADER_LABEL}
    should be equal as strings  ${ElementText}  Our Amazing Team  ignore_case=true

