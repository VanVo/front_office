*** Settings ***
Resource  ./PO/Landing.robot
Resource  ./PO/TopNav.robot
Resource  ./PO/Team.robot

*** Keywords ***
Go to Landing page
    Landing.Navigate to
    Landing.Verify Page Load

Go to "Team" page
    TopNav.Select "Team" page
    Team.Verify Page Load

Validate "Team" page
    Team.Validate "Team" Page Content